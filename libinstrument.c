/*

  instrument-calls - simple instrumentation tool library

  Copyright (C) 2023 INRIA

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <dlfcn.h>
#include <stdatomic.h>
#include <link.h>

#define FLAG_ENTER 1
#define FLAG_EXIT 2

static bool started;
static const char *logfname;
static FILE *logfile;
static volatile atomic_flag mutex = ATOMIC_FLAG_INIT;
#define try_acquire(m) (!(atomic_flag_test_and_set(m)))
#define acquire(m) while (atomic_flag_test_and_set(m))
#define release(m) atomic_flag_clear(m)

static void dump_log(FILE *logfile, unsigned char flags, uint32_t addr, uint32_t addr_site, const char *so_name, const char *so_site_name) {
  uint32_t len_so = strlen(so_name);
  uint32_t len_so_site = strlen(so_site_name);
  fwrite(&flags, sizeof(flags), 1, logfile);
  fwrite(&addr, sizeof(addr), 1, logfile);
  fwrite(&addr_site, sizeof(addr_site), 1, logfile);
  fwrite(&len_so, sizeof(len_so), 1, logfile);
  fwrite(&len_so_site, sizeof(len_so_site), 1, logfile);
  if (len_so > 0) {
    fwrite(so_name, len_so, 1, logfile);
  }
  if (len_so_site > 0) {
    fwrite(so_site_name, len_so_site, 1, logfile);
  }
}

void __cyg_profile_func_enter (void *this_fn,
                               void *call_site) {
  Dl_info info;
  Dl_info info_site;
  struct link_map *map; 
  struct link_map *map_site;

  if (!try_acquire(&mutex)) {
    return;
  }
  if (!started) {
    started = true;
    logfname = getenv("TRACE_FNAME");
    if (logfname != NULL) {
      logfile = fopen(logfname, "wb");
    }
  }
  if (logfile == NULL) {
    goto exit;
  }
  int ret = dladdr1(this_fn, &info, (void **)&map, RTLD_DL_LINKMAP);
  int ret_site = dladdr1(call_site, &info_site, (void **)&map_site, RTLD_DL_LINKMAP);
  if (ret && map != NULL && map->l_name != NULL &&
      ret_site && map_site->l_name != NULL) {
    dump_log(logfile, FLAG_ENTER, (uint32_t)(uintptr_t)(this_fn - map->l_addr), (uint32_t)(uintptr_t)(call_site - map_site->l_addr),
             map->l_name, map_site->l_name);
  }
 exit:
  release(&mutex);
}

void __cyg_profile_func_exit  (void *this_fn,
                               void *call_site) {
  Dl_info info;
  Dl_info info_site;
  struct link_map *map; 
  struct link_map *map_site;

  if (!try_acquire(&mutex)) {
    return;
  }
  if (logfile == NULL) {
    goto exit;
  }
  int ret = dladdr1(this_fn, &info, (void **)&map, RTLD_DL_LINKMAP);
  int ret_site = dladdr1(call_site, &info_site, (void **)&map_site, RTLD_DL_LINKMAP);
  if (ret && map != NULL && map->l_name != NULL &&
      ret_site && map_site->l_name != NULL) {
    dump_log(logfile, FLAG_EXIT, (uint32_t)(uintptr_t)(this_fn - map->l_addr), (uint32_t)(uintptr_t)(call_site - map_site->l_addr),
             map->l_name, map_site->l_name);
  }
 exit:
  release(&mutex);
}
