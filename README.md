Instrumentation utils
=====================

Simple library and utilities for generating traces of function execution.


Installation
------------

Requirements:
- python 3.7+ and pip3
- perl 5
- gcc 9+

For instance on Ubuntu/Debian systems, install requirements with:

    sudo apt install perl python3 python3-pip gcc

The install python dependencies:

    pip3 install -r requirements.txt

And compile with:

    make


This will generate a `libinstrument.so` shared object to be used with the
compiler `-finstrument-functions` flag.


Usage
-----

Then compile your code with `-finstrument-functions`, add debug (`-g`) in order
to have line information (this is not required though):

    gcc -o instrumented.exe -g -O2 -finstrument-functions examples/instrumented.c

Run with `LD_PRELOAD=./libinstrument.so` and `TRACE_FNAME=<trace_file>`,
for instance:

    env LD_PRELOAD=./libinstrument.so TRACE_FNAME=trace.log ./instrumented.exe
    LOG: Creating arrays
    LOG: Randomizing arrays
    A has 1 / 3 ones
    B has 3 / 5 ones
    LOG: Discarding arrays

This will generate a binary trace into `trace.log`, one can decode and print
the trace with the `print-trace.py EXE TRACE` util:

    ./print-trace.py instrumented.exe trace.log
    CALL: main at instrumented.c:70
      CALL: msg at instrumented.c:65 FROM main at instrumented.c:74
      EXIT: msg at instrumented.c:65 FROM main at instrumented.c:74
      CALL: array_new at instrumented.c:23 FROM main at instrumented.c:75
    ...


One can also print the stack trace with:

    ./print-trace.py --stack instrumented.exe trace.log
    ...
      1:      6: main;array_count;array_size
      3:      7: main;array_count;array_get
      1:      6: main;array_count;array_size
      5:      7: main;array_count;array_get
      2:      8: main;array_size
      1:      0: main;msg
      2:      9: main;array_del

The first column is the number of continuous occurences of the stack frame, the second the stack frame unique id and the
last the list of functions for the frame.

Last a flame graph compatible folded stack trace output can be produced with:

    ./print-trace.py --flame instrumented.exe trace.log
    ...
    main;array_count;array_size 1
    main;array_count;array_get 3
    main;array_count;array_size 1
    main;array_count;array_get 5
    main;array_size 2
    main;msg 1
    main;array_del 2

In turn this can be used to produce a SVG flame graph with:

    ./print-trace.py --with-lines --flame instrumented.exe trace.log >instrumented.flame.txt
    utils/flamegraph.pl --flamechart --hash instrumented.flame.txt >instrumented.flame.svg

The generated SVG file can be viewed with your browser:

    x-www-browser instrumented.flame.svg

Below is the generated file (download and reopen in browser for interactive zooms):

![SVG Call stacks](examples/instrumented.flame.svg "Call stacks over time")


Examples
--------

Refer to [examples README](examples/README.md) for more examples (in particular how to
instrument QEMU).

Greetings
---------

The `utils/flamegraph.pl` script is a a copy of the original flame graph script
from https://github.com/brendangregg/FlameGraph at revision `d9fcc272b6` (2022/08/18).

Refer to the awesome flame graph site by Brendan Gregg at https://www.brendangregg.com/flamegraphs.html.
