#!/usr/bin/env python3

# instrument-calls - simple instrumentation tool library

# Copyright (C) 2023 INRIA

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import sys
import struct
import logging
import argparse
import numpy as np
import pickle
from pathlib import Path
from dataclasses import dataclass

try:
    from elftools.elf.elffile import ELFFile
    from elftools.elf.sections import SymbolTableSection
except ModuleNotFoundError as e:
    print(f'ERROR: {e}', file=sys.stderr)
    print(f'    Some modules are missing, please install first required modules with:', file=sys.stderr)
    print(f'        pip install -r {Path(__file__).parent / "requirements.txt"}', file=sys.stderr)
    sys.exit(1)

logger = logging.getLogger(__name__)

@dataclass
class Symbol():
    address: int
    name: str
    file: str = None
    line: int = 0

@dataclass
class AddrLine():
    address: int
    file: str
    line: int

class Decoder():
    VERSION = '1.0.0'
    def __init__(self, soname):
        self._get_decoder(soname)

    def _get_decoder(self, soname):
        base = Path(soname).name
        sopickle = Path(f'{base}.dec.pickle')
        if (sopickle.exists() and
            sopickle.stat().st_mtime > Path(soname).stat().st_mtime):
            with open(sopickle, 'rb') as f:
                decoder = pickle.load(f)
                if (decoder.VERSION == self.VERSION and
                    decoder.soname == soname):
                    self._copy_from(decoder)
                    return
        self._init_decoder(soname)
        with open(sopickle, 'wb') as f:
            pickle.dump(self, f)

    def _copy_from(self, other):
        self.soname = other.soname
        self.symbols = other.symbols
        self.symbols_addrs = other.symbols_addrs
        self.addrlines = other.addrlines
        self.addrlines_addrs = other.addrlines_addrs
        
    def _init_decoder(self, soname):
        with open(soname, 'rb') as sofile:
            elffile = ELFFile(sofile)
            self.soname = soname
            self.symbols = self._get_symbols(elffile)
            self.symbols_addrs = np.array([s.address for s in self.symbols], dtype='int')
            self.addrlines = self._get_addrlines(elffile)
            self.addrlines_addrs = np.array([l.address for l in self.addrlines], dtype='int')
            if self.addrlines:
                for sym in self.symbols:
                    idx = self._find_address(self.addrlines_addrs, sym.address)
                    if idx is not None:
                        sym.file = self.addrlines[idx].file
                        sym.line = self.addrlines[idx].line
    

    @classmethod
    def _get_symbols(cls, elffile):
        section = elffile.get_section_by_name('.symtab')
        if not section or not isinstance(section, SymbolTableSection):
            symbols = []
        else:
            symbols = [Symbol(s.entry.st_value, s.name) for s in section.iter_symbols()
                       if s.entry.st_info.type == 'STT_FUNC' and isinstance(s.entry.st_shndx, int)]
        symbols = sorted(symbols, key=lambda s: s.address)
        return symbols

    @classmethod
    def _get_addrlines(cls, elffile):
        if not elffile.has_dwarf_info():
            return []
        dwarfinfo = elffile.get_dwarf_info()
        addrlines = []
        for CU in dwarfinfo.iter_CUs():
            lineprog = dwarfinfo.line_program_for_CU(CU)
            delta = 1 if lineprog.header.version < 5 else 0
            prevstate = None
            for entry in lineprog.get_entries():
                if entry.state is None:
                    continue
                need_update = True
                if prevstate:
                    if (entry.state.end_sequence or
                        not (prevstate.address == entry.state.address or
                             (prevstate.address < entry.state.address and
                              prevstate.file == entry.state.file and prevstate.line == entry.state.line))):
                        fname = lineprog['file_entry'][prevstate.file - delta].name.decode()
                        logger.debug(f'    FNAME: {prevstate.file} {fname}')
                        addrlines.append(AddrLine(prevstate.address, fname, prevstate.line))
                    else:
                        need_update = False
                if entry.state.end_sequence:
                    prevstate = None
                elif need_update:
                    prevstate = entry.state

        addrlines = sorted(addrlines, key=lambda l: l.address)
        return addrlines

    @classmethod
    def _find_address(cls, addrs, addr):
        if len(addrs) == 0:
            return None
        idx = np.searchsorted(addrs, addr)
        if idx == 0 and addr < addrs[idx]:
            return None
        if idx < len(addrs) and addrs[idx] == addr:
            return idx
        return idx -1

    def find_symbol(self, addr):
        idx = self._find_address(self.symbols_addrs, addr)
        if idx is None:
            return None
        sym = self.symbols[idx]
        idx = self._find_address(self.addrlines_addrs, addr)
        if idx is not None:
            sym.file = self.addrlines[idx].file
            sym.line = self.addrlines[idx].line
        return sym
    
    def dump_symbols(self, file=sys.stdout):
        for s in self.symbols:
            if s.file:
                file.write(f'SYM: {s.address:x} {s.name} {s.file}:{s.line}\n')
            else:
                file.write(f'SYM: {s.address:x} {s.name}\n')

    def dump_addrlines(self, file=sys.stdout):
        for al in self.addrlines:
            file.write(f'LINE: {al.address:x} {al.file}:{al.line}\n')

class TraceDecoder():
    def __init__(self, fname, exe):
        self.fname = fname
        self.exe = exe
        self.decoders = {}
        self.symbols = {}
        self.prints = {}
        self.stacks_prints = {}
        self.stacks_idxs = {}
        self.stacks_num = 0

    def record_gen(self):
        RECORD_LEN = 17 # 1 + 4 + 4 + 4 + 4
        with open(self.fname, 'rb') as f:
            while True:
                buf = f.read(RECORD_LEN)
                if len(buf) < RECORD_LEN:
                    break
                flag, addr, addr_site, so_len, so_site_len = struct.unpack_from('<BLLLL', buf)
                if so_len > 0:
                    so_name = f.read(so_len)
                    if len(so_name) < so_len:
                        break
                    so_name = so_name.decode()
                else:
                    so_name = self.exe

                if so_site_len > 0:
                    so_site_name = f.read(so_site_len)
                    if len(so_site_name) < so_site_len:
                        break
                    so_site_name = so_site_name.decode()
                else:
                    so_site_name = self.exe
            
                yield flag, addr, addr_site, so_name, so_site_name

    def get_decoder(self, soname):
        if soname not in self.decoders:
            self.decoders[soname] = Decoder(soname)
        return self.decoders[soname]

    def find_symbol(self, soname, addr):
        key = (soname, addr)
        if key not in self.symbols:
            decoder = self.get_decoder(soname)
            self.symbols[key] = decoder.find_symbol(addr)
        return self.symbols[key]

    def find_pretty_symbol(self, soname, addr):
        key = (soname, addr)
        if key not in self.prints:
            sym = self.find_symbol(soname, addr)
            if sym is None or sym.name is None:
                pretty = ''
            elif sym.file is not None:
                pretty = f'{sym.name} at {sym.file}:{sym.line}'
            else:
                pretty = sym.name
            self.prints[key] = pretty
        return self.prints[key]

    def print_trace(self, file=sys.stdout):
        ind = 0
        for flag, addr, addr_site, soname, soname_site in self.record_gen():
            sym_pretty = self.find_pretty_symbol(soname, addr)
            if sym_pretty:
                if flag & 1:
                    entry = 'CALL'
                    next_ind = ind + 1
                else:
                    entry = 'EXIT'
                    ind = max(0, ind - 1)
                    next_ind = ind
                site_pretty = self.find_pretty_symbol(soname_site, addr_site)
                if site_pretty:
                    file.write(f'{" "*ind}{entry}: {sym_pretty} FROM {site_pretty}\n')
                else:
                    file.write(f'{" "*ind}{entry}: {sym_pretty}\n')
                ind = next_ind

    def stack_gen_all(self):
        stack = []
        for flag, addr, addr_site, soname, soname_site in self.record_gen():
            if flag & 1:
                stack.append((addr, addr_site, soname, soname_site))
                yield tuple(stack)
            else:
                if len(stack) > 0:
                    stack.pop()  # TODO search call site

    def stack_gen_folded(self):
        stack = []
        exiting = False
        for flag, addr, addr_site, soname, soname_site in self.record_gen():
            if flag & 1:
                stack.append((addr, addr_site, soname, soname_site))
                exiting = False
            else:
                if not exiting:
                    yield tuple(stack)
                    exiting = True
                if len(stack) > 0:
                    stack.pop()  # TODO search call site

    def stack_gen(self, folded=False):
        if folded:
            return self.stack_gen_folded()
        else:
            return self.stack_gen_all()

    def find_pretty_stack(self, stack, with_lines=False):
        key = (stack, with_lines)
        if key not in self.stacks_prints:
            syms = []
            for addr, addr_site, soname, soname_site in stack:
                sym = self.find_symbol(soname, addr)
                if sym is None or sym.name is None:
                    syms.append('[unknown]')
                elif with_lines and sym.file is not None:
                    syms.append(f'{sym.name} at {sym.file}:{sym.line}')
                else:
                    syms.append(sym.name)
            pretty = ';'.join(syms)
            self.stacks_prints[key] = (pretty, with_lines)
        else:
            pretty, _ = self.stacks_prints[key]
        return pretty

    def stack_pretty_gen(self, with_lines=False):
        idxs = {}
        num = 1
        last = None
        last_idx = None
        for stack in self.stack_gen(folded=True):
            stack_pretty = self.find_pretty_stack(stack, with_lines=with_lines)
            if stack_pretty not in idxs:
                idxs[stack_pretty] = len(idxs)
            idx = idxs[stack_pretty]
            if last != stack_pretty:
                if last is not None:
                    yield (num, last_idx, last)
                last_idx = idx
                last = stack_pretty
                num = 1
            else:
                num += 1
        if last is not None:
            yield (num, last_idx, last)
        
    def print_stack_trace(self, with_lines=False, file=sys.stdout):
        for num, idx, stack in self.stack_pretty_gen(with_lines=with_lines):
            file.write(f'{num:3}: {idx:6}: {stack}\n')

    def print_flame_chart(self, with_lines=False, file=sys.stdout):
        for num, idx, stack in self.stack_pretty_gen(with_lines=with_lines):
            file.write(f'{stack} {num}\n')


def main(argv):
    parser = argparse.ArgumentParser(description='Pretty-print call trace')
    parser.add_argument('--stack', action='store_true', help='Print stack trace')
    parser.add_argument('--flame', action='store_true', help='Print flame chart')
    parser.add_argument('--with-lines', action='store_true', help='Add file:line info to stack/flame traces')
    parser.add_argument('--debug', action='store_true', help='Debug output')
    parser.add_argument('EXE', help='Executable file')
    parser.add_argument('TRACE', help='Binary trace file')

    args = parser.parse_args(argv[1:])

    logging.basicConfig()
    logger.setLevel(logging.INFO)
    if args.debug:
        logger.setLevel(logging.DEBUG)

    tracer = TraceDecoder(args.TRACE, args.EXE)
    if args.stack:
        tracer.print_stack_trace(with_lines=args.with_lines)
    elif args.flame:
        tracer.print_flame_chart(with_lines=args.with_lines)
    else:
        tracer.print_trace()
    return 0

if __name__ == '__main__':
    try:
        sys.exit(main(sys.argv))
    except ModuleNotFoundError as e:
        print(f'ERROR: {e}', file=sys.stderr)
        print(f'  Some modules are missing, please install first required modules with:', file=sys.stderr)
        print(f'    pip install -r requirements.txt', file=sys.stderr)
        sys.exit(1)
    
