Examples
========

Basic Example
-------------

The `instrumented.flame.svg` is the stack chart of a simple example of
arrays initializations and search, produced with:

    gcc -o instrumented.exe -g -O2 -finstrument-functions instrumented.c
    env LD_PRELOAD=../libinstrument.so TRACE_FNAME=trace.log ./instrumented.exe
    ../print-trace.py --with-lines --flame instrumented.exe trace.log >instrumented.flame.txt
    ../utils/flamegraph.pl --flamechart --hash instrumented.flame.txt >instrumented.flame.svg

Visualize it with:

    x-www-browser instrumented.flame.svg

Shown here (download and reopen in browser for interactive zooms):

![SVG Call stacks](instrumented.flame.svg "Call stacks over time for instrumented.exe")


QEMU instrumentation
--------------------

The `qemu.flame.avx.svg` is the stack chart of a fragment of executiuon of QEMU on some
file.

In order to build qemu with instrumentation, first get qemu code,
tested here on `stable-8.0` QEMU branch:

    git clone -b stable-8.0 --depth 1 https://git.qemu.org/git/qemu.git
    cd qemu
    mkdir build

Then fix QEMU code in order to be compatible with instrumentation:

    sed -i 's|if defined(__OPTIMIZE__)|if !defined(INSTRUMENT_FUNCTIONS) \&\& defined(__OPTIMIZE__)|' include/qemu/osdep.h

Configure and compile QEMU:

    cd build
    ../configure --extra-cflags="-DINSTRUMENT_FUNCTIONS -finstrument-functions" --target-list=x86_64-linux-user --prefix=$PWD/install
    ninja
    ninja install

Then, for instance use qemu to emulate the basic example program generated above, but this time instrument qemu itself
(i.e. not the emulated example program, but the process which emulates it):

    env TRACE_FNAME=trace.qemu.log LD_PRELOAD=../libinstrument.so qemu/build/install/bin/qemu-x86_64 -U LD_PRELOAD ./instrumented.exe

From the generated `trace.qemu.log`, one can now generate the call traces or the stack traces, for instance generate the
stack trace for a flame graph with:

    ../print-trace.py --with-lines --flame qemu/build/install/bin/qemu-x86_64 trace.qemu.log >instrumented.qemu.flame.txt

Note that the `flamegraph.pl` script does not manage well very large stack trace files (>100000) lines, hence first take a fragment
of the full stack trace, for instance 2000 lines around the first call to `disas_insn_new`:

    grep -B 1000 -A 1000 disas_insn_new instrumented.qemu.flame.txt  | head -2000 >instrumented.qemu.flame.part.txt

Then one can generate the flame graph and view it:

    ../utils/flamegraph.pl --flamechart --hash  instrumented.qemu.flame.part.txt >instrumented.qemu.flame.part.svg

Shown here (download and reopen in browser for interactive zooms):

![SVG QEMU Call stacks](instrumented.qemu.flame.part.svg "Call stacks over time for instrumented.exe emulated with QEMU")
