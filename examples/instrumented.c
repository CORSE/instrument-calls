#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>

static int rnd_seed;
#define RND_MAX ((int)0x7fffffff)
void rnd_init(int seed)
{
  assert(RND_MAX <= INT_MAX);
  rnd_seed = seed;
}

int rnd() {
  if (rnd_seed == 0) {
    rnd_init(1);
  }
  rnd_seed = (int)(((uint64_t)rnd_seed * 48271) % ((uint64_t)RND_MAX + 1));
  return rnd_seed;
}

int *array_new(int size) {
  int *array = (int *)malloc(sizeof(int)*(size+1));
  array[0] = size;
  return array;
}

void array_del(int *array) {
  free(array);
}

int array_size(const int *array) {
  return array[0];
}

void array_set(int *array, int idx, int val) {
  assert(idx >= 0);
  assert(idx < array[0]);
  array[idx+1] = val;
}

int array_get(const int *array, int idx) {
  assert(idx >= 0);
  assert(idx < array[0]);
  return array[idx+1];;
}

void array_random(int *array, int max) {
  int size = array_size(array);
  for (int i = 0; i < size; i++) {
    array_set(array, i, rnd() % (max + 1));
  }
}

int array_count(const int *array, int val) {
  int count = 0;
  int size = array_size(array);
  for (int i = 0; i < size; i++) {
    if (array_get(array, i) == val) {
      count++;
    }
  }
  return count;
}

void msg(const char *msg) {
  printf("LOG: %s\n", msg);
}


int main(int argc, char *args) {
  int *A, *B;

  msg("Creating arrays");
  A = array_new(3);
  B = array_new(5);

  msg("Randomizing arrays");
  array_random(A, 2);
  array_random(B, 2);
  
  int count_A_1 = array_count(A, 1);
  int count_B_1 = array_count(B, 1);

  printf("A has %d / %d ones\n", count_A_1, array_size(A));
  printf("B has %d / %d ones\n", count_B_1, array_size(B));

  msg("Discarding arrays");
  array_del(A);
  array_del(B);

  return 0;
}
