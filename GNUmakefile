#
# instrument-calls - simple instrumentation tool library
#
# Copyright (C) 2023 INRIA
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#

CC=gcc
LD=$(CC)
CFLAGS=-O2 -Wall
LDFLAGS=-O2 -Wall
LIBS=

MAKE_DEPS=GNUmakefile make.cache

SO=libinstrument.so
SO_SRCS=libinstrument.c
SO_OBJS=$(SO_SRCS:.c=.so.o)
SO_CFLAGS=$(CFLAGS) -fPIC
SO_LDFLAGS=$(LDFLAGS)
SO_LIBS=$(LIBS) -ldl

all: so

so: $(SO)

$(SO): $(MAKE_DEPS)
$(SO): CFLAGS := $(SO_CFLAGS)
$(SO): LDFLAGS := $(SO_LDFLAGS)
$(SO): LIBS := $(SO_LIBS)
$(SO): $(SO_OBJS)
	$(CC) -shared -o $@ $(CFLAGS) $(SO_OBJS) $(LDFLAGS) $(LIBS)

$(SO_OBJS): $(MAKE_DEPS)
$(SO_OBJS): CFLAGS := $(SO_CFLAGS)
$(SO_OBJS): %.so.o: %.c
	$(CC) -c -o $@ $(CFLAGS) $*.c

make.cache: .FORCE
	@echo "CC=$(CC)" >>make.tmp.cache
	@echo "LD=$(LD)" >>make.tmp.cache
	@echo "CFLAGS=$(CFLAGS)" >make.tmp.cache
	@echo "LDFLAGS=$(LDFLAGS)" >>make.tmp.cache
	@echo "LIBS=$(LIBS)" >>make.tmp.cache
	@diff -q make.tmp.cache make.cache >/dev/null 2>&1 || mv make.tmp.cache make.cache
	@rm -f make.tmp.cache

clean:
	rm -f *.exe *.so *.o *.cache

distclean: clean
	rm -f *~

.FORCE:
.SUFFIXES:
.PHONY: all clean distclean
